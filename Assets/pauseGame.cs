﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pauseGame : MonoBehaviour {

    public Transform canvas;
    public Transform player;
    public Transform spawn;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (canvas.gameObject.activeInHierarchy == false)
            {
                canvas.gameObject.SetActive(true);
                Time.timeScale = 0;
                if (player.GetComponent<CharacterController>())
                player.GetComponent<CharacterController>().enabled = false;
                spawn.GetComponent<EnemySpawn>().enabled = false;
            }
            else
            {
                canvas.gameObject.SetActive(false);
                Time.timeScale = 1;
                if (player.GetComponent<CharacterController>())
                player.GetComponent<CharacterController>().enabled = true;
                spawn.GetComponent<EnemySpawn>().enabled = true;
            }
        }
    }
}
