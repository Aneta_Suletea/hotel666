﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeyBind : MonoBehaviour {
    public Button backButton;
    private Dictionary<string, KeyCode> keys = new Dictionary<string, KeyCode>();
    public Text right, left, accelerate, decelerate;
    private GameObject currentKey;

    private Color32 normal = new Color32(39, 171, 249, 255);
    private Color32 selected = new Color32(239, 116, 36, 255);

    void Start () {
        keys.Add("Right", KeyCode.D);
        keys.Add("Left", KeyCode.A);
        keys.Add("Accelerate", KeyCode.Z);
        keys.Add("Decelerate", KeyCode.X);

        right.text = keys["Right"].ToString();
        left.text = keys["Left"].ToString();
        accelerate.text = keys["Accelerate"].ToString();
        decelerate.text = keys["Decelerate"].ToString();

        backButton = backButton.GetComponent<Button>();
        backButton.onClick.AddListener(backPress);

    }
	

	void Update () {
        if (Input.GetKeyDown(keys["Right"]))
        {
            Debug.Log("Right");
        }
        if (Input.GetKeyDown(keys["Left"]))
        {
            Debug.Log("Left");
        }
        if (Input.GetKeyDown(keys["Accelerate"]))
        {
            Debug.Log("Accelerate");
        }
        if (Input.GetKeyDown(keys["Decelerate"]))
        {
            Debug.Log("Decelerate");
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            backPress();
        }
    }

    void OnGUI()
    {
        if (currentKey != null)
        {
            Event e = Event.current;
            if (e.isKey)
            {
                keys[currentKey.name] = e.keyCode;
                currentKey.transform.GetChild(0).GetComponent<Text>().text = e.keyCode.ToString();
                currentKey.GetComponent<Image>().color = normal;
                currentKey = null;
            }
        }
    }

    public void ChangeKey(GameObject clicked)
    {
        if(currentKey != null)
        {
            currentKey.GetComponent<Image>().color = normal;
        }
        currentKey = clicked;
        currentKey.GetComponent<Image>().color = selected;
    }

    private void backPress()
    {
        Debug.Log("Back press");
        this.gameObject.SetActive(false);
    }

}
