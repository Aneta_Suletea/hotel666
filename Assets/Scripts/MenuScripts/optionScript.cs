﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class optionScript : MonoBehaviour {
    public Button soundButton;
    public Button controlButton;
    public Button backButton;
    public Canvas sound;
    public Canvas control;

    private void Start()
    {
        soundButton = soundButton.GetComponent<Button>();
        soundButton.onClick.AddListener(soundPress);
        controlButton = controlButton.GetComponent<Button>();
        controlButton.onClick.AddListener(controlPress);
        backButton = backButton.GetComponent<Button>();
        backButton.onClick.AddListener(backPress);
    }
    private void soundPress()
    {
        Debug.Log("Sound press");
        sound.gameObject.SetActive(true);
    }
    private void controlPress()
    {
        Debug.Log("Controls press");
        control.gameObject.SetActive(true);
    }
    private void backPress()
    {
        Debug.Log("Back press");
        this.gameObject.SetActive(false);
    }
}
