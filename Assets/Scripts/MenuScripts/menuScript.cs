﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class menuScript : MonoBehaviour {
    public Button newGame;
    public Button controls;
    public Button help;
    public Button exit;
    public Canvas controlCanvas;
    public Canvas menuCanvas;
    bool controlOpen = false;
	// Use this for initialization
	void Start () {
        newGame = newGame.GetComponent<Button>();
        controls = controls.GetComponent<Button>();
        help = help.GetComponent<Button>();
        exit = exit.GetComponent<Button>();
    }
    public void newGamePress()
    {
        Debug.LogError("New Game press");
        Time.timeScale = 1;        
        SceneManager.LoadScene("StartMenu");
    }
    public void helpPress()
    {
        Debug.LogError("Help press");
    }
    public void toggleControlsPress()
    {
        controlOpen = !controlOpen;
        controlCanvas.gameObject.SetActive(controlOpen);
        menuCanvas.gameObject.SetActive(!controlOpen);
        Debug.LogError("Controls press");
    }
	public void exitPress()
    {
        Debug.LogError("Exit press");
        Application.Quit();
        
    }
	// Update is called once per frame
	void Update () {
		
	}
}
