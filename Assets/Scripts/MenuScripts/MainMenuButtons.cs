﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuButtons : MonoBehaviour {
    public Button newGame;
    public Button options;
    public Button exit;

    public Canvas saveSlot;
    public Canvas optionsCanvas;
    public Canvas characterChoose;

    private void Start()
    {
        Time.timeScale = 0;
    }
    public void newGamePress()
    {
        saveSlot.gameObject.SetActive(true);
        this.gameObject.SetActive(false);
    }
    public void backPress(Canvas lastCanvas)
    {
        this.gameObject.SetActive(false);
        lastCanvas.gameObject.SetActive(true);
    }
    public void soundPress()
    {
        optionsCanvas.gameObject.SetActive(true);
        this.gameObject.SetActive(false);
    }
    public void exitButtonPress()
    {
        Application.Quit();
    }
   public void saveSlotPressed()
    {
        this.gameObject.SetActive(false);
        characterChoose.gameObject.SetActive(true);
       
    }
  
    public void startPress()
    {
        optionsCanvas.gameObject.SetActive(false);
        characterChoose.gameObject.SetActive(false);
        saveSlot.gameObject.SetActive(false);
        this.gameObject.SetActive(false);
        Time.timeScale = 1;
        
    }

}
