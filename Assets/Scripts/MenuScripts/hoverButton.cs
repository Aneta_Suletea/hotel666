﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class hoverButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public Sprite hoveredButton;
    private Sprite currentSprite;
    private Image imageComponent;

	void Start () {
        imageComponent =GetComponent<Image>();
        currentSprite = imageComponent.sprite;
	}
	
    public void OnPointerEnter(PointerEventData eventData)
    {
        
        imageComponent.sprite = hoveredButton;
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        imageComponent.sprite = currentSprite;
    }
   
}
