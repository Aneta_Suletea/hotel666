﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitScreen : MonoBehaviour {
    public GameObject initialisationScreen;
    public GameObject loadingScreen;
    public GameObject mainMenu;
    float timer;
    private void Start()
    {
        initialisationScreen.SetActive(false);
        loadingScreen.SetActive(true);
        mainMenu.SetActive(false);
        timer = 0.0f;
    }
    void Update () {
        if (initialisationScreen.activeSelf) {
           if(Input.anyKey)
           {
                mainMenu.SetActive(true);
                initialisationScreen.SetActive(false);
                this.gameObject.SetActive(false);

            }
        }
        if(loadingScreen.activeSelf)
        {
            if (timer >= 2)
            {
                initialisationScreen.SetActive(true);
                loadingScreen.SetActive(false);
            }
            timer += Time.deltaTime;
        }
    }
}
