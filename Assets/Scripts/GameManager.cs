﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    private static GameManager instance;
    public GameObject player;
    private GameManager()
    {

    }
    public static GameManager Instance
    {
        get
        {
            instance = GameObject.FindObjectOfType(typeof(GameManager)) as GameManager;
            return instance;
        }
    }
    public delegate void LooseLowestSpeed();
    public event LooseLowestSpeed LooseSpeed;
    public delegate void MinusHp(GameObject enemy=null);
    public event MinusHp SubstractHp;
    public void Initial_SubstractHp(GameObject enemy = null)
    {
        //toate astea sunt necesare la init in start
    }
    public delegate void PlusHp(GameObject healer=null);
    public event PlusHp AddHp;
    public void Initial_AddHp(GameObject healer = null)
    {
        //toate astea sunt necesare la init in start
    }
    public void Initial_LooseSpeed()
    {
        //dummy, necesar initializarii
    }
    public void TriggerLooseSpeed()
    {
        GameManager.Instance.LooseSpeed();
    }
    public void TriggerMinusHp(GameObject enemy)
    {
        GameManager.Instance.SubstractHp(enemy);
    }
    public void TriggerGameFinished()
    {
        GameManager.Instance.gameFinished();
    }
    public delegate void GameFinished();
    public event GameFinished gameFinished;
    public void Initial_GameFinished()
    {
        Time.timeScale = 0;
    }
    // Use this for initialization
    void Start () {
        GameManager.Instance.LooseSpeed += Initial_LooseSpeed;
        GameManager.Instance.SubstractHp += Initial_SubstractHp;
        GameManager.Instance.AddHp += Initial_AddHp;
        GameManager.Instance.gameFinished += Initial_GameFinished;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
