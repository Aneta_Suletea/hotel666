﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class EnemyController : MonoBehaviour
{
    float dist;

    private Vector3[] wayPointsList = new Vector3[5];
    private int currentWay = 0;
    private int size = 0;
    Vector3 targetWayPoint;
    Vector3 playerSpeed;
    private float speed = 20f;
    float timer = 0f;
    bool isInside = false;
    int noOfWays;
    float delay = 0f;
    bool directTarget = false;
    GameObject player;
    bool timeLimitExceeded = false;
    float playerStopTimer = 0f;
    bool startCounting = false;
    private bool isEnemyControllerEnabled = true;

    int i = 0;
    int maxStepsBeforeDelay;

    void Start()
    {
        player = GameManager.Instance.player;
        GenerateWayPoints();
        targetWayPoint = wayPointsList[0];
        maxStepsBeforeDelay = Random.Range(1, 5);
        GameManager.Instance.gameFinished += GameFinished;
    }
    public void GameFinished()
    {
        isEnemyControllerEnabled = false;
    }
    void FixedUpdate()
    {

        timer += Time.deltaTime;

        if (timer >= 6)
        {
            //destroys object
            Destroy(gameObject);
            //reset timer
            timer = 0f;
        }
        if (startCounting)
        {
            delay -= Time.deltaTime;
            if (delay <= 0)
            {
                startCounting = false;
            }
        }
    }
    private void Update()
    {
        if (isEnemyControllerEnabled)
        {
            if (delay <= 0 || currentWay == 0)
            {
                walk();
            }
            else if (currentWay != 0)
            {

                //take max 5 more steps before the delay applies
                while (i < maxStepsBeforeDelay)
                {
                    walk();
                    i++;
                }
                if (i >= maxStepsBeforeDelay)
                {
                    startCounting = true;
                }
            }
        }
    }

    void walk()
    {
        if (currentWay != 0)
        {
            transform.forward = Vector3.RotateTowards(transform.forward, targetWayPoint - transform.position, speed * Time.deltaTime, 0.0f);
        }
        transform.position = Vector3.MoveTowards(transform.position, targetWayPoint, speed * Time.deltaTime);
        if (transform.position == targetWayPoint)
        {
            currentWay++;
            if (currentWay < size)
            {
                targetWayPoint = wayPointsList[currentWay] + transform.position;
            }
        }
    }
    float calculateSpeed()
    {
        int playerGear = player.GetComponent<SimpleCarController>().getGear;
        switch (playerGear)
        {
            case 0:
                return 8f;
            case 1:
                return 8f;
            case 2:
                return 10f;
            case 3:
                return 12f;
            case 4:
                return 14f;
            case 5:
                return 16f;
            case 6:
                return 18f;
            default:
                return 8f;
        }

    }
    void GenerateWayPoints()
    {
        //first two directions are mandatory

        //first wayPoint for moving on the same level with Character
        wayPointsList[0] = new Vector3(transform.position.x, 0.5f, transform.position.z);
        //decrease speed for moving towards character
        speed = calculateSpeed();
        //second wayPoint is for targeting character
        wayPointsList[1] = GeneratePath(false);
        size = 2;
        float dist = Vector3.Distance(player.transform.position, transform.position);
        if (dist >= 6)
        {
            if (dist >= 10)
            {
                //it will bounce 
                noOfWays = Random.Range(2, 4);
            }
            else
            {
                //75 percent to bounce
                int p = Random.Range(1, 4);
                if (!(p == 4))
                {
                    noOfWays = Random.Range(1, 3);
                }
                else
                {
                    noOfWays = 0;
                }
            }
        }
        else
        {
            //it may only bounce once
            noOfWays = Random.Range(0, 1);
        }
    }
    Vector3 GeneratePath(bool alternativePath)
    {

        if (player)
        {
           
            if (player.GetComponent<SimpleCarController>().playerStopped)
            {
                Vector3 enemyPosition = transform.position;
                enemyPosition.y = 0.5f;
                Vector3 targetPlayer = player.transform.position - enemyPosition;
                return targetPlayer +targetPlayer.normalized* Random.Range(0, 10);
            }
            else
            {
                //gets the distance in front of the player
                Vector3 playerForward = player.transform.position + player.transform.forward;
                //move boundary
                Vector3 moveBoundary = (playerForward - transform.position);
                Vector3 secondMoveBoundary;
                Vector3 destination;
                float rotation;
                //generates random rotation angle for the enemy
                int enemyRotation = Random.Range(45, 90);
                if (transform.position.x < player.transform.position.x)
                {
                    //enemy is on the left of the player
                    //second boundary is - degrees to the left of the enemy
                    rotation = player.transform.eulerAngles.y - enemyRotation;
                    secondMoveBoundary = Quaternion.Euler(0, rotation, 0) * moveBoundary;
                    //get the middle vector between the boundaries
                    destination = Vector3.Cross(moveBoundary, secondMoveBoundary);
                }
                else
                {
                    //enemy is on the right
                    //second boundary is + degrees to the right of the enemy
                    rotation = player.transform.eulerAngles.y + enemyRotation;
                    secondMoveBoundary = Quaternion.Euler(0, rotation, 0) * moveBoundary;
                    //get the middle vector between the boundaries
                    destination = -Vector3.Cross(moveBoundary, secondMoveBoundary);
                }
                //if the player is moving backward
                if (player.transform.eulerAngles.y > 135 && player.transform.eulerAngles.y < 225)
                {
                    destination *= -1;
                }

                //set y to the current y position
                destination.y = 0.5f;

                //draws rays 
                // Debug.Break();
                // Vector3 x = new Vector3(transform.position.x, 0.5f, transform.position.z);
                ///  Debug.DrawRay(x, moveBoundary * 15, Color.red);
                // Debug.DrawRay(x, destination, Color.green);
                // Debug.DrawRay(x, secondMoveBoundary * 15, Color.black);
                //generates a random distance for the enemy to go
                int distanceToGo = Random.Range(3, 10);
                if (alternativePath)
                {
                    if (Vector3.Distance(player.transform.position, transform.position) > 3)
                    {
                        //10% to target player
                        int rand = Random.Range(0, 10);
                        if (rand == 7 && directTarget == false)
                        {
                            moveBoundary.y = 0.5f;
                            directTarget = true;
                            return moveBoundary + moveBoundary.normalized * distanceToGo;
                        }
                    }
                    else
                    {
                        destination.z += player.transform.forward.z * 3;


                    }
                    return -(destination + destination.normalized * distanceToGo);

                }
                return destination + destination.normalized * distanceToGo;
            }

        }
        else
        {
            Debug.LogError("Could not find player");
            return new Vector3(0, 0, 0);
        }
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            GameManager.Instance.TriggerMinusHp(this.gameObject);

        }
        if(other.tag == "Floor")
        {
            isInside = true;
            //may have a chance for delay 20%
            if (Random.Range(0, 4) == 2)
            {
                delay = Random.Range(1, 4);
            }
        }
        if(other.tag == "Walls")
        {
            if (!isInside)
            {
                isInside = true;
                //may have a chance for delay 20%
                if (Random.Range(0, 4) == 2)
                {
                    delay = Random.Range(1, 4);
                }
            } else
            if (currentWay>=1 && noOfWays > 0)
            {    
                    wayPointsList[currentWay + 1] = GeneratePath(true);
                    currentWay++;
                    targetWayPoint = wayPointsList[currentWay] + transform.position;
                    targetWayPoint.y = 0.5f;       
                    noOfWays--;
            }
            
        }
    }
}
