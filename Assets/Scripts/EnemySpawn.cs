﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour {

    //arrays for enemies
    public GameObject[] enemyPrefab;
    public  GameObject[] enemyClone;

    //player info
    public Transform player;

    //spawn distance
    public float spawnMaxDistance = 10;
    public float spawnMinDistance = 5;
    public float xSpawnCoordinateOffset = 5;
    bool spawned = false;
    private bool enabledSpawner = true;
    
    //timer variables
    float timer = 0f;
    public float maxSpawnTime = 15;
    public float minSpawnTime = 7;
    float currentSpawnTime;

    void increaseLimitSpawn()
    {
        int playerGear = player.GetComponent<SimpleCarController>().getGear;
        switch (playerGear)
        {
            case 0:
            case 1:
            case 2:
                spawnMinDistance++;
                spawnMaxDistance++;
                break;
            case 3:
                spawnMinDistance += 2;
                spawnMaxDistance += 2;
                break;
            case 4:
                spawnMinDistance += 3;
                spawnMaxDistance += 3;
                break;
            case 5:
                spawnMinDistance += 4;
                spawnMaxDistance += 4;
                break;
            case 6:
                spawnMinDistance += 5;
                spawnMaxDistance += 5;
                break;
        }

    }
    public void GameFinished()
    {
        enabledSpawner = false;
    }
    private void Start()
    {
        //initializes a spawn timeout that is a random value between minSpawnTime and maxSpawnTime
        increaseLimitSpawn();
        currentSpawnTime = Random.Range(minSpawnTime, maxSpawnTime);
        if (GameManager.Instance)
        {
            GameManager.Instance.gameFinished += GameFinished;
        }
    }
    private void Update()
    {
        
        timer += Time.deltaTime;
        if (timer >= currentSpawnTime)
        {
            CalculateSpawnPoint();
            //reset timer
            timer = 0f;
            //calculate another spawn Time
            currentSpawnTime = Random.Range(minSpawnTime, maxSpawnTime);
        }
    }
    /**
     * Function responsible for Spawning a random enemy from array
    **/
    void Spawn(Vector3 spawnLocation)
    {
        if (enabledSpawner)
        {
            //generates a random enemy from array
            int enemyNo = Random.Range(0, enemyPrefab.Length);
            //spawns Enemy
            enemyClone[enemyNo] = Instantiate(enemyPrefab[enemyNo], spawnLocation, Quaternion.Euler(0, 0, 0)) as GameObject;
            enemyClone[enemyNo].tag = "Enemy";
        }
    }
    /**
     *  Calculates a random number in an area that is duable for spawn
     **/
    void CalculateSpawnPoint()
    {

        float zDist = Random.Range(spawnMinDistance, spawnMaxDistance);
        float xDist = Random.Range(-xSpawnCoordinateOffset, xSpawnCoordinateOffset); 

        //generates a random spawn point for x coordinate
        Vector3 zCoordinate = player.transform.forward * zDist;
        Vector3 xCoordinate =player.transform.right * xDist;

        Vector3 spawnLocation =player.transform.position + zCoordinate + xCoordinate;
        spawnLocation.y = -3;;

        Spawn(spawnLocation);

    }


}
