﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
public class CameraFollow : MonoBehaviour
{
    GameObject player;

    private Vector3 m_camDirection;
    private float sphereCastRadius = 0.1f;           // the radius of the sphere used to test for object between camera and target
                                                     // private Transform m_Pivot;
                                                     // private Transform initial_Pivot;// the point at which the camera pivots around
    private float m_OriginalDist;             // the original distance to the camera before any modification are made
                                             
    private Ray m_Ray = new Ray();                        // the ray used in the lateupdate for casting between the camera and the target
    private RaycastHit[] m_Hits;              // the hits between the camera and the target
    private RayHitComparer m_RayHitComparer;  // variable to compare raycast hit distances

    private Vector3 offset;
    private bool optimalPointFound = false;
    private bool isUp = false;
    private bool seesPlayer = true;
    private bool changeFacing = false;
    Vector3 initOffset;
    public Transform initialTransform;
    void Start()
    {
        player = GameManager.Instance.player;

        offset = transform.position - player.transform.position;
        initOffset = offset;
        m_camDirection = transform.position.normalized;
        m_OriginalDist = transform.position.magnitude;
        // create a new RayHitComparer
        m_RayHitComparer = new RayHitComparer();
    }
    private void Update()
    {
        
        if (playerNotSeen(transform))
        {
            seesPlayer = false;
            if (!optimalPointFound && !isUp)
            {
                Vector3 off;
                float dist = Vector3.Distance(transform.position, player.transform.position);
                off = -player.transform.forward * dist;
                off.y = transform.position.y;
                initialTransform.position = player.transform.position + off; ;
                if (!CheckCollision(initialTransform) && !playerNotSeen(initialTransform))
                {
                    offset = off;
                    initOffset = off;
                    optimalPointFound = true;
                }
                else
                {
                    optimalPointFound = false;
                }
            }
        }
        else
        {
            seesPlayer = true;
        }
        if (transform.position == player.transform.position + offset)
        {
            //cam ok
            optimalPointFound = false;
            isUp = false;
        }
        if (transform.position.x == player.transform.position.x && transform.position.z == player.transform.position.z)
        {
            isUp = true;
        }
        if (playerFacingCamera() && !changeFacing && !isUp)
        {
            Vector3 off;
            float dist = Vector3.Distance(transform.position, player.transform.position);
            off = -player.transform.forward * dist;
            off.y = transform.position.y;
            offset = off;
            changeFacing = true;
        }
        initialTransform.position = Vector3.MoveTowards(initialTransform.position, player.transform.position + initOffset, Time.deltaTime * 10);
            transform.position = Vector3.MoveTowards(transform.position, player.transform.position + offset, Time.deltaTime * 10);
            transform.LookAt(player.transform);
            initialTransform.LookAt(player.transform);
        

    }
    private void LateUpdate()
    {
        if (!optimalPointFound && !seesPlayer)
        {
            if (!isUp)
            {
                offset = Vector3.zero;
                offset.y = 3.2f;
            }
           
        }
        if (isUp)
        {
            if (!CheckCollision(initialTransform) && !playerNotSeen(initialTransform))
            {
                offset = initOffset;
            }
        }
    }
    private bool CheckCollision(Transform m_Pivot)
    {
        // initially set the target distance
        float targetDist = m_OriginalDist;

        m_Ray.origin = m_Pivot.position + m_Pivot.forward * sphereCastRadius;
        m_Ray.direction = -m_Pivot.forward;


        // initial check to see if start of spherecast intersects anything
        var cols = Physics.OverlapSphere(m_Ray.origin, sphereCastRadius);

        bool initialIntersect = false;
        bool hitSomething = false;

        // loop through all the collisions to check if something we care about
        for (int i = 0; i < cols.Length; i++)
        {
            if (cols[i].tag == "Walls")
            {
                initialIntersect = true;
                break;
            }
        }

        // if there is a collision
        if (initialIntersect)
        {
            m_Ray.origin += m_Pivot.forward * sphereCastRadius;

            // do a raycast and gather all the intersections
            m_Hits = Physics.RaycastAll(m_Ray, m_OriginalDist - sphereCastRadius);
        }
        else
        {
            // if there was no collision do a sphere cast to see if there were any other collisions
            m_Hits = Physics.SphereCastAll(m_Ray, sphereCastRadius, m_OriginalDist + sphereCastRadius);
        }

        // sort the collisions by distance
        Array.Sort(m_Hits, m_RayHitComparer);

        // set the variable used for storing the closest to be as far as possible
        float nearest = 0.5f;

        // loop through all the collisions
        for (int i = 0; i < m_Hits.Length; i++)
        {
            // only deal with the collision if it was closer than the previous one
            if (m_Hits[i].distance < nearest && m_Hits[i].collider.tag == "Walls")
            {
                // change the nearest collision to latest
                nearest = m_Hits[i].distance;
                targetDist = -m_Pivot.InverseTransformPoint(m_Hits[i].point).z;
                hitSomething = true;
               //Debug.DrawRay(m_Ray.origin, -m_Pivot.forward * (targetDist + sphereCastRadius), Color.red);
            }
        }
        return hitSomething;
    }

    bool playerNotSeen(Transform m_transform)
    {
         Ray ray = new Ray();
         ray.origin = m_transform.position;
        ray.direction = (player.transform.position - m_transform.position);

        Vector3 directionToPlayer = (player.transform.position - m_transform.position).normalized;
        RaycastHit[] coverHits;
        coverHits = Physics.RaycastAll(ray, 10);
        if (coverHits.Length > 0)
        {
            for (int i = 0; i < coverHits.Length; i++)
            {
                bool playerIsSeen = false;
                if (coverHits[i].transform.gameObject.tag == "Player")
                {
                    playerIsSeen = true;
                }
                if (coverHits[i].transform.gameObject.tag == "Walls" && !playerIsSeen)
                {
                    return true;

                }
            }
        }
        return false;
    }

    bool playerFacingCamera()
    {
        float angle = Mathf.Abs(Vector3.Angle(transform.forward, player.transform.forward));
        if (angle>135 && angle<225)
        {
            return true;     
        }
        changeFacing = false;
        return false;
    }
}

// comparer for check distances in ray cast hits
public class RayHitComparer : IComparer
{
    public int Compare(object x, object y)
    {
        return ((RaycastHit)x).distance.CompareTo(((RaycastHit)y).distance);
    }
}