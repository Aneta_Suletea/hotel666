﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

[RequireComponent(typeof(SimpleCarController))]
public class SimpleCarUserControl : MonoBehaviour {

    private SimpleCarController m_Car; // the car controller we want to use
    public float sensitivity_reduction=5f;
    private void Awake()
    {
        // get the car controller
        m_Car = GetComponent<SimpleCarController>();
    }
    private bool forward = false, backward = false;    
    private void Update()
    {
        /*if (CrossPlatformInputManager.GetButtonDown("Fire1"))
        {
            Vector3 mousePoint = CrossPlatformInputManager.mousePosition;
            mousePoint.z = m_Car.GetMaxMarkerCastDistance();
            Ray ray = Camera.main.ScreenPointToRay(mousePoint);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, m_Car.GetMaxMarkerCastDistance()))
            {
                Debug.Log("Target found at " + mousePoint);
                m_Car.CreateMarkerAtMousePoint(hit.point);
            }
        }*/
        // pass the input to the car!
        float v = 0;
        float h = CrossPlatformInputManager.GetAxis("Horizontal");
        h = h / (float)sensitivity_reduction;
        //Debug.Log("H = " + h); //intre -1 si 1
        bool gear_up = CrossPlatformInputManager.GetButtonDown("Forward");
        bool gear_down = CrossPlatformInputManager.GetButtonDown("Backward");
        if (gear_up)
        {
            v = 1;
            HudManager.Instance.SetMovementImageToForward();
        }
        if (gear_down)
        {
            v = -1;
            HudManager.Instance.SetMovementImageToBackward();
        }
        if (gear_up && gear_down) v = 0;
        if (v==0)
        {
            HudManager.Instance.SetMovementImageToStop();
        }
        if (h!=0&&v> Mathf.Abs(h) / 2f)
        v = v - (Mathf.Abs(h)/2f);
        //Debug.Log("Gear up : " + gear_up + " Gear down " + gear_down);
        /*if (h!=0||v!=0)
        Debug.Log(" h = " + h + " v = " + v);*/
#if !MOBILE_INPUT
        float handbrake = CrossPlatformInputManager.GetAxis("Jump");
        m_Car.GearChanging(gear_up, gear_down);
        m_Car.Move(h, v, v, handbrake);
#else
            m_Car.Move(h, v, v, 0f);
#endif        
        if (m_Car.GetComponent<Rigidbody>().velocity==Vector3.zero)
        {
            //player stopped
            //Debug.Log("Player full stop");
            m_Car.StopBackgroundMusicOnPlayerStop();
        }
        else
        {
            //Debug.Log("Current speed = " + m_Car.CurrentSpeed);
            m_Car.StartBackgroundMusicOnPlayerMove();
        }
    }
}
