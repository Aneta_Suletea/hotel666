﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour {

    public float maxTorque = 100f;
    public float steerForce = 1f;
    public Transform centerOfMass;
    public int Gear = 0, minGear = 0;

    public WheelCollider[] wheelColliders = new WheelCollider[3];
    public Transform[] tireMeshes = new Transform[3];

    private Rigidbody m_rigidBody;

    void Start()
    {
        m_rigidBody = GetComponent<Rigidbody>();
        m_rigidBody.centerOfMass = centerOfMass.localPosition;
    }

    void Update()
    {
        UpdateMeshesPositions();
        ChangeSpeed();
        float steer = Input.GetAxis("Horizontal");
        float accelerate = Input.GetAxis("Vertical");

        float finalAngle = steer * 45f;
        wheelColliders[0].steerAngle = finalAngle;

        for (int i = 0; i < 3; i++)
        {
            wheelColliders[i].motorTorque = maxTorque;
        }
    }

    void FixedUpdate()
    {
       
    }

    void ChangeSpeed()
    {
        if (Input.GetButtonUp("Fire1") && Gear < 5)
        {
            Gear++;
        }
        
        if (Input.GetButtonUp("Fire2") && Gear > minGear)
        {
            Gear--;
        }

        switch (Gear)
        {
            case 0:
                maxTorque = 0;
                break;
            case 1:
                maxTorque = 20;
                break;
            case 2:
                maxTorque = 40;
                break;
            case 3:
                maxTorque = 60;
                break;
            case 4:
                maxTorque = 80;
                break;
        }

        //doar ca sa vezi daca merge functia
        //dupa poti sa stergi if-ul asta
        if (Input.GetKeyUp(KeyCode.Space))
        {
            Damage();
        }
    }

    void Damage()
    {
        minGear++;

        if (Gear < minGear)
        {
            Gear = minGear;
        }
        if (minGear == 5)
        {
           // GameOver();
            Gear = 0;
        }
    }

    void UpdateMeshesPositions()
    {
        for (int i = 0; i < 3; i++)
        {
            Quaternion quat;
            Vector3 pos;
            wheelColliders[i].GetWorldPose(out pos, out quat);

            tireMeshes[i].position = pos;
            tireMeshes[i].rotation = quat;
        }
    }

}
