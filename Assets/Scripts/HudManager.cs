﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HudManager : MonoBehaviour {

    // Use this for initialization
    private static HudManager instance;
    [SerializeField] private Text gameTimer;
    [SerializeField] private Sprite forwardArrow;
    [SerializeField] private Sprite noArrow;
    [SerializeField] private Sprite backwardArrow;
    [SerializeField] private Image movementImage;
    public Color enabled_color=new Color(0,1,0,1),
                 disabled_color=new Color(1,0,0,1),
                 current_color=new Color(0,0,1,1);
    private GameObject[] speed_panels = new GameObject[7];
    private GameObject[] heal_panels = null;
    private HudManager()
    {

    }
    public static HudManager Instance
    {
        get
        {
            instance = GameObject.FindObjectOfType(typeof(HudManager)) as HudManager;
            return instance;
        }
    }
    public void ActivateSpeed(int index)
    {
        if (speed_panels!=null)
        {
            Text txt=speed_panels[index].GetComponentInChildren<Text>();
            if (txt != null)
            txt.color = enabled_color;
        }
    }
    public void GameOver()
    {
        gameTimer.text = "GAME OVER";
        gameTimer.color = disabled_color;
    }
    public void LooseHp()
    {
        Debug.Log("Damage taken, updating health panels");
        Text current_text;
        int i = 0;
        for (i=0;i<heal_panels.Length;i++)
        {
            current_text=heal_panels[i].GetComponentInChildren<Text>();            
            if (current_text!=null&&current_text.text== "Available")
            {
                current_text.text = "Damaged";
                current_text.color = disabled_color;                
                break;
            }
        }
    }
    public void DeActivateSpeed(int index)
    {
        if (speed_panels != null)
        {
            Text txt = speed_panels[index].GetComponentInChildren<Text>();
            if (txt!=null)
            txt.color = this.disabled_color;
        }
    }
    public void SetMovementImageToForward()
    {
        movementImage.sprite = forwardArrow;
    }
    public void SetMovementImageToBackward()
    {
        movementImage.sprite = backwardArrow;
    }
    public void SetMovementImageToStop()
    {
        movementImage.sprite = noArrow;
    }
    public void SetCurrentSpeed(int index,bool[] active_speeds,int max_speeds)
    {
        if (speed_panels != null)
        {
            /*Debug.Log("Enabled color " + enabled_color);
            Debug.Log("Disabled color " + disabled_color);
            Debug.Log("Current color " + current_color);*/
            int i;
            Text txt_i;
            for (i = 0; i < max_speeds; i++)
            {
                if (speed_panels[i]==null)
                {
                    Debug.LogError("Panel nr " + i + " is null. SpeedPanel length : "+speed_panels.Length);                    
                }
                else
                {
                    //Debug.Log("Panel nr " + i + " found");
                    txt_i = speed_panels[i].GetComponentInChildren<Text>();
                    if (active_speeds[i])
                    {
                        if (txt_i != null)
                            txt_i.color = this.enabled_color;
                    }
                    else
                    {
                        if (txt_i != null)
                            txt_i.color = this.disabled_color;
                    }
                }                
            }
            Text txt = speed_panels[index].GetComponentInChildren<Text>();
            if (txt != null)
            {
                txt.color = this.current_color;
                //Debug.Log("Current txt " + txt.text+" Current color: "+txt.color);
            }
        }
    }
    private int CompareByNames(GameObject x, GameObject y)
    {
        return x.name.CompareTo(y.name);
    }
    public void GameFinished()
    {
        gameTimer.text = "SUCCESS!!";
        gameTimer.color = enabled_color;
    }
    void Awake () {
        speed_panels = GameObject.FindGameObjectsWithTag("SpeedPanels");
        Array.Sort(speed_panels,CompareByNames);
        heal_panels = GameObject.FindGameObjectsWithTag("HealthPanels");
        if (heal_panels != null)
            Array.Sort(heal_panels, CompareByNames);
        else
            Debug.LogError("Health panels not found");
        if (GameManager.Instance)
        {
            GameManager.Instance.gameFinished += GameFinished;
        }
    }
	
	// Update is called once per frame
	void Update () {
        
	}
    private void OnGUI()
    {
        if (Time.timeScale != 0)
        {
            float timer = Time.time;
            int minutes = Mathf.FloorToInt(timer / 60F);
            int seconds = Mathf.FloorToInt(timer - minutes * 60);
            string niceTime = string.Format("{0:0}:{1:00}", minutes, seconds);
            gameTimer.text = niceTime;
        }
    }
}
