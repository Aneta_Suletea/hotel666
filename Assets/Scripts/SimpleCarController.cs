﻿using System;
using UnityEngine;

//namespace UnityStandardAssets.Vehicles.Car
//{
internal enum CarDriveType
{
    FrontWheelDrive,
    RearWheelDrive,
    FourWheelDrive
}

internal enum SpeedType
{
    MPH,
    KPH
}

public class SimpleCarController : MonoBehaviour
{
    private const int MAX_NR_WHEELS = 4;
    private const int MAX_NR_GEARS = 7;
    [SerializeField] private AudioSource movementSound = null;
    [SerializeField] private AudioSource damageSound = null;
    [SerializeField] private AudioSource crashSound = null;
    [SerializeField] private AudioSource randomDoorKnockSound = null;
    [SerializeField] private int randomDoorKnockSoundStartInterval = 0;
    [SerializeField] private int randomDoorKnockSoundEndInterval = 0;
    private int chosenRandomKnockSound = 0;
    private float timerRandomKnockSound=0;
    //-------------------------------
    [SerializeField] private AudioSource randomDoor1Sound = null;
    [SerializeField] private int randomDoor1SoundStartInterval = 0;
    [SerializeField] private int randomDoor1SoundEndInterval = 0;
    private int chosenRandomDoor1Sound = 0;
    private float timerRandomDoor1Sound = 0;
    //--------------------------------
    [SerializeField] private AudioSource randomDoor2Sound = null;
    [SerializeField] private int randomDoor2SoundStartInterval = 0;
    [SerializeField] private int randomDoor2SoundEndInterval = 0;
    private int chosenRandomDoor2Sound = 0;
    private float timerRandomDoor2Sound = 0;
    //---------------------------------------
    [SerializeField] private AudioSource randomDoor3Sound = null;
    [SerializeField] private int randomDoor3SoundStartInterval = 0;
    [SerializeField] private int randomDoor3SoundEndInterval = 0;
    private int chosenRandomDoor3Sound = 0;
    private float timerRandomDoor3Sound = 0;
    //---------------------------------
    [SerializeField] private int number_of_wheels = 4;
    [SerializeField] private CarDriveType m_CarDriveType = CarDriveType.FourWheelDrive;
    [SerializeField] private WheelCollider[] m_WheelColliders = new WheelCollider[MAX_NR_WHEELS];
    [SerializeField] private GameObject[] m_WheelMeshes = new GameObject[MAX_NR_WHEELS];
    //[SerializeField] private WheelEffects[] m_WheelEffects = new WheelEffects[number_of_wheels];
    [SerializeField] private Vector3 m_CentreOfMassOffset;
    [SerializeField] private float m_MaximumSteerAngle;
    [Range(0, 1)] [SerializeField] private float m_SteerHelper; // 0 is raw physics , 1 the car will grip in the direction it is facing
    [Range(0, 1)] [SerializeField] private float m_TractionControl; // 0 is no traction control, 1 is full interference
    [SerializeField] private float m_FullTorqueOverAllWheels;
    [SerializeField] private float m_ReverseTorque;
    [SerializeField] private float m_MaxHandbrakeTorque;
    [SerializeField] private float m_Downforce = 100f;
    [SerializeField] private SpeedType m_SpeedType;
    [SerializeField] private float m_Topspeed = 200;
    public int NoOfGears = 7;
    [SerializeField] private float m_RevRangeBoundary = 1f;
    [SerializeField] private float m_SlipLimit;
    [SerializeField] private float m_BrakeTorque;
    [SerializeField] private float[] torque_by_speed = new float[MAX_NR_GEARS];
    [SerializeField] private float torque_frame_by_frame;
    [SerializeField] private float reverse_torque_by_speed=-100;
    private bool is_reverse = false;
    private float current_Torque = 0;
    private float target_Torque = 0;
    public bool[] active_speeds=new bool[MAX_NR_GEARS];
    public int PlayerHp = 6;

    private Quaternion[] m_WheelMeshLocalRotations;
    private Vector3 m_Prevpos, m_Pos;
    private float m_SteerAngle;
    private int current_GearNumber;
    private float m_GearFactor;
    private float m_OldRotation;
    private float m_CurrentTorque;
    private Rigidbody m_Rigidbody;
    private const float k_ReversingThreshold = 0.01f;

    public bool Skidding { get; private set; }
    public float BrakeInput { get; private set; }
    public float CurrentSteerAngle { get { return m_SteerAngle; } }
    public float CurrentSpeed { get { return m_Rigidbody.velocity.magnitude * 2.23693629f; } }
    public float MaxSpeed { get { return m_Topspeed; } }
    public float Revs { get; private set; }
    public float AccelInput { get; private set; }
    [SerializeField] private GameObject markerPrefab;
    [SerializeField] private int max_raycast_marker_distance = 10;
    public const int MAX_NR_MARKERS = 6;
    private int current_marker_index = 0;
    private GameObject[] markers=new GameObject[MAX_NR_MARKERS];
	
	private bool breakLimitExceeded;
    private float timer = 0.0f;
    public bool playerStopped { get { return breakLimitExceeded; } }
    public int getGear { get { return current_GearNumber; } }

    public void OnCollisionEnter(Collision collision)
    {
        //Debug.Log("Collision detectat la player, tag = "+ collision.gameObject.tag);
        if (collision.gameObject.CompareTag("Walls"))
        {
            crashSound.Play();
        }
    }
    public void SubstractHP(GameObject enemy = null)
    {
        Debug.Log("Player taking damage");
        PlayerHp = PlayerHp - 1;
        if (PlayerHp <= 0)
        {
            //Game over
            Time.timeScale = 0; //freeze game
            HudManager.Instance.GameOver();
            PlayerHp = 0;
        }
        HudManager.Instance.LooseHp();
        LooseLowestSpeed();
        damageSound.Play();
    }
    public void AddHP(GameObject healer = null)
    {
        PlayerHp = PlayerHp + 1;
    }
    private void Update()
    {
        
    }
    private void FixedUpdate()
    {
        System.Random rnd = new System.Random();
        timerRandomKnockSound += Time.deltaTime;
        if (timerRandomKnockSound>=chosenRandomKnockSound)
        {
            randomDoorKnockSound.Play();
            timerRandomKnockSound = 0f;
            chosenRandomKnockSound = rnd.Next(randomDoorKnockSoundStartInterval, randomDoorKnockSoundEndInterval);
        }
        //-------------------------------------
        timerRandomDoor1Sound += Time.deltaTime;
        if (timerRandomDoor1Sound >= chosenRandomDoor1Sound)
        {
            randomDoor1Sound.Play();
            timerRandomDoor1Sound = 0f;
            chosenRandomDoor1Sound = rnd.Next(randomDoor1SoundStartInterval, randomDoor1SoundEndInterval);
        }
        //--------------------------------------
        timerRandomDoor2Sound += Time.deltaTime;
        if (timerRandomDoor2Sound >= chosenRandomDoor2Sound)
        {
            randomDoor2Sound.Play();
            timerRandomDoor2Sound = 0f;
            chosenRandomDoor2Sound = rnd.Next(randomDoor2SoundStartInterval, randomDoor2SoundEndInterval);
        }
        //---------------------------------------------
        timerRandomDoor3Sound += Time.deltaTime;
        if (timerRandomDoor3Sound >= chosenRandomDoor3Sound)
        {
            randomDoor3Sound.Play();
            timerRandomDoor3Sound = 0f;
            chosenRandomDoor3Sound = rnd.Next(randomDoor3SoundStartInterval, randomDoor3SoundEndInterval);
        }
        //-------------------------------------------
        if (m_Rigidbody.velocity == Vector3.zero)
        { 
            timer += Time.deltaTime;
            if (timer >= 5)
            {
                breakLimitExceeded = true;
                timer = 0.0f;
            }
 
 
        }
        else
        {
            breakLimitExceeded = false;
            timer = 0f;
        }
    }
	
    public int GetMaxMarkerCastDistance()
    {
        return max_raycast_marker_distance;
    }
    public void CreateMarkerAtMousePoint(Vector3 position)
    {        
        if (current_marker_index < markers.Length-1)
        {
            Debug.Log("Create marker at " + position);
            current_marker_index = current_marker_index + 1;
            markers[current_marker_index] = Instantiate(markerPrefab, position, Quaternion.identity);
            markers[current_marker_index].SetActive(true);
            markers[current_marker_index].name = "Marker_" + current_marker_index;
        }
        else
        {
            Debug.Log("Max markers reached");
        }
    }
    public void LooseLowestSpeed()
    {
        int i;
        for (i=1;i<NoOfGears-1;i++)
        {
            if (active_speeds[i]==true)
            {
                active_speeds[i] = false;
                break;
            }
        }
        if (HudManager.Instance!=null)
        HudManager.Instance.DeActivateSpeed(i);
    }
    public void GameFinished()
    {
        GetComponent<Rigidbody>().velocity = Vector3.zero;
    }
    // Use this for initialization
    private void Start()
    {
		 breakLimitExceeded = false;
        if (GameManager.Instance != null)
        {
            //Debug.Log("Intra aici");
            
        }
        if (GameManager.Instance != null)
        {
            //Debug.Log("Intra aici");
            GameManager.Instance.LooseSpeed += LooseLowestSpeed;
            GameManager.Instance.AddHp+=AddHP;
            GameManager.Instance.SubstractHp += SubstractHP;
            GameManager.Instance.gameFinished += GameFinished;
        }
        //else Debug.LogError("Red alert");
        if (HudManager.Instance!=null)
        HudManager.Instance.SetCurrentSpeed(0,active_speeds,NoOfGears);
        //movementSound = GameObject.FindGameObjectWithTag("BackgroundMusic").GetComponent<AudioSource>();
        m_WheelMeshLocalRotations = new Quaternion[number_of_wheels];
        for (int i = 0; i < number_of_wheels; i++)
        {            
            m_WheelMeshLocalRotations[i] = m_WheelMeshes[i].transform.localRotation;
            //Debug.Log(m_WheelMeshLocalRotations[i]);
        }
        m_WheelColliders[0].attachedRigidbody.centerOfMass = m_CentreOfMassOffset;

        m_MaxHandbrakeTorque = float.MaxValue;

        m_Rigidbody = GetComponent<Rigidbody>();
        if (m_Rigidbody==null)
        {
            Debug.LogError("RIGIDBODY NOT FOUND");
            Application.Quit();
        }
        m_CurrentTorque = m_FullTorqueOverAllWheels - (m_TractionControl * m_FullTorqueOverAllWheels);
    }

    public void StopBackgroundMusicOnPlayerStop()
    {
        if (movementSound.isPlaying)
        {
            movementSound.Pause();
        }
    }
    public void StartBackgroundMusicOnPlayerMove()
    {
        if (!movementSound.isPlaying)
        {
            movementSound.UnPause();
        }
    }
    private int SeekNextAvailableGearUp(int current_gear)
    {
        int i;
        int next_gear=-1;
        if (current_gear == NoOfGears)
        {
            next_gear = current_gear;
        }
        else
        {
            for (i = current_gear + 1; i < NoOfGears; i++)
            {
                if (active_speeds[i] == true)
                {
                    next_gear = i;
                    break;
                }
            }
        }
        if (next_gear == -1) next_gear = current_gear;
        return next_gear;
    }
    private int SeekNextAvailableGearDown(int current_gear)
    {
        int i;
        int next_gear = -1;
        if (current_gear == 0)
        {
            next_gear = current_gear;
        }
        else
        {
            for (i = current_gear - 1; i >= 0; i--)
            {
                if (active_speeds[i] == true)
                {
                    next_gear = i;
                    break;
                }
            }
        }
        if (next_gear == -1) next_gear = current_gear;
        return next_gear;
    }
    public void GearChanging(bool gear_up,bool gear_down)
    {
        /*float f = Mathf.Abs(CurrentSpeed / MaxSpeed);
        float upgearlimit = (1 / (float)NoOfGears) * (current_GearNumber + 1);
        float downgearlimit = (1 / (float)NoOfGears) * current_GearNumber;

        if (current_GearNumber > 0 && f < downgearlimit)
        {
            current_GearNumber--;
        }

        if (f > upgearlimit && (current_GearNumber < (NoOfGears - 1)))
        {
            current_GearNumber++;
        }*/
        if (current_GearNumber==0&&gear_down)
        {
            is_reverse = true; //Aneta nu schimba ordinea, e important
            target_Torque = reverse_torque_by_speed;
        }
        if (is_reverse&&gear_up)
        {
            is_reverse = false;            
        }
        if (!is_reverse)
        {
            if (gear_up && current_GearNumber < NoOfGears)
            {
                current_GearNumber = SeekNextAvailableGearUp(current_GearNumber);
                HudManager.Instance.SetCurrentSpeed(current_GearNumber, active_speeds, NoOfGears);
                target_Torque = torque_by_speed[current_GearNumber];
                //Debug.Log("Current gear : " + current_GearNumber + " NoOfGears : " + NoOfGears + " GearUp : " + gear_up + " GearDown : " + gear_down);
            }
            if (gear_down && current_GearNumber > 0)
            {
                current_GearNumber = SeekNextAvailableGearDown(current_GearNumber);
                HudManager.Instance.SetCurrentSpeed(current_GearNumber, active_speeds, NoOfGears);
                target_Torque = torque_by_speed[current_GearNumber];
                //Debug.Log("Current gear : " + current_GearNumber + " NoOfGears : " + NoOfGears + " GearUp : " + gear_up + " GearDown : " + gear_down);
            }
        }
    }


    // simple function to add a curved bias towards 1 for a value in the 0-1 range
    private static float CurveFactor(float factor)
    {
        return 1 - (1 - factor) * (1 - factor);
    }


    // unclamped version of Lerp, to allow value to exceed the from-to range
    private static float ULerp(float from, float to, float value)
    {
        return (1.0f - value) * from + value * to;
    }


    private void CalculateGearFactor()
    {
        float f = (1 / (float)NoOfGears);
        // gear factor is a normalised representation of the current speed within the current gear's range of speeds.
        // We smooth towards the 'target' gear factor, so that revs don't instantly snap up or down when changing gear.
        var targetGearFactor = Mathf.InverseLerp(f * current_GearNumber, f * (current_GearNumber + 1), Mathf.Abs(CurrentSpeed / MaxSpeed));
        m_GearFactor = Mathf.Lerp(m_GearFactor, targetGearFactor, Time.deltaTime * 5f);
    }


    private void CalculateRevs()
    {
        // calculate engine revs (for display / sound)
        // (this is done in retrospect - revs are not used in force/power calculations)
        CalculateGearFactor();
        var gearNumFactor = current_GearNumber / (float)NoOfGears;
        var revsRangeMin = ULerp(0f, m_RevRangeBoundary, CurveFactor(gearNumFactor));
        var revsRangeMax = ULerp(m_RevRangeBoundary, 1f, gearNumFactor);
        Revs = ULerp(revsRangeMin, revsRangeMax, m_GearFactor);
    }


    public void Move(float steering, float accel, float footbrake, float handbrake)
    {
        //steering, accel, footbrake valori intre -1 si 1.
        //accel este egal cu footbrake
        for (int i = 0; i < number_of_wheels; i++)
        {
            Quaternion quat;
            Vector3 position;
            m_WheelColliders[i].GetWorldPose(out position, out quat);
            m_WheelMeshes[i].transform.position = position;
            m_WheelMeshes[i].transform.rotation = quat;
        }

        //clamp input values
        steering = Mathf.Clamp(steering, -1, 1);
        AccelInput = accel = Mathf.Clamp(accel, 0, 1);
        BrakeInput = footbrake = -1 * Mathf.Clamp(footbrake, -1, 0);
        handbrake = Mathf.Clamp(handbrake, 0, 1);

        //Set the steer on the front wheels.
        //Assuming that wheels 0 and 1 are the front wheels.
        m_SteerAngle = steering * m_MaximumSteerAngle;
        m_WheelColliders[0].steerAngle = m_SteerAngle;
        //Debug.Log(m_WheelColliders[0].steerAngle);
        if (number_of_wheels>3)
        m_WheelColliders[1].steerAngle = m_SteerAngle;

        SteerHelper();
        ApplyDrive(accel, footbrake);
        CapSpeed();

        //Set the handbrake.
        //Assuming that wheels 2 and 3 are the rear wheels.
        if (handbrake > 0f)
        {
            var hbTorque = handbrake * m_MaxHandbrakeTorque;
            m_WheelColliders[number_of_wheels-1].brakeTorque = hbTorque;
            m_WheelColliders[number_of_wheels-2].brakeTorque = hbTorque;
        }


        CalculateRevs();
        //GearChanging();

        AddDownForce();
        CheckForWheelSpin();
        TractionControl();
    }


    private void CapSpeed()
    {
        float speed = m_Rigidbody.velocity.magnitude; //metrii pe secunda
        switch (m_SpeedType)
        {
            case SpeedType.MPH:

                speed *= 2.23693629f;
                if (speed > m_Topspeed)
                    m_Rigidbody.velocity = (m_Topspeed / 2.23693629f) * m_Rigidbody.velocity.normalized;
                break;

            case SpeedType.KPH:
                speed *= 3.6f;
                if (speed > m_Topspeed)
                    m_Rigidbody.velocity = (m_Topspeed / 3.6f) * m_Rigidbody.velocity.normalized;
                break;
        }
    }

    //float maxTorque = 0;
    private void ApplyDrive(float accel, float footbrake)
    {
        float wheel_torque=0;
        if (current_Torque<target_Torque)
        {
            current_Torque = current_Torque + torque_frame_by_frame;
        }
        if (current_Torque > target_Torque)
        {
            current_Torque = current_Torque - torque_frame_by_frame;
        }
        switch (m_CarDriveType)
        {
            case CarDriveType.FourWheelDrive:
                wheel_torque = m_CurrentTorque / (float)number_of_wheels;
                for (int i = 0; i < number_of_wheels; i++)
                {
                    m_WheelColliders[i].motorTorque = current_Torque;
                }
                break;

            case CarDriveType.FrontWheelDrive:
                wheel_torque = m_CurrentTorque / (float)(MAX_NR_WHEELS - number_of_wheels);                
                if (number_of_wheels > 3)
                    m_WheelColliders[0].motorTorque = m_WheelColliders[1].motorTorque = current_Torque;
                else
                {
                    m_WheelColliders[0].motorTorque = current_Torque;
                }
                //Debug.Log("Current speed : " + CurrentSpeed);
                break;

            case CarDriveType.RearWheelDrive:
                wheel_torque = (m_CurrentTorque / 2f);
                m_WheelColliders[number_of_wheels - 1].motorTorque = m_WheelColliders[number_of_wheels - 2].motorTorque = current_Torque;
                break;

        }

        for (int i = 0; i < number_of_wheels; i++)
        {
            if (CurrentSpeed > 5 && Vector3.Angle(transform.forward, m_Rigidbody.velocity) < 50f)
            {
                m_WheelColliders[i].brakeTorque = m_BrakeTorque * footbrake * (current_GearNumber/(float)NoOfGears);
            }
            else if (footbrake > 0)
            {
                m_WheelColliders[i].brakeTorque = 0f;
                m_WheelColliders[i].motorTorque = -m_ReverseTorque * footbrake * (current_GearNumber/(float)NoOfGears);
            }
        }
        /*if (maxTorque < current_Torque) maxTorque = current_Torque;
        else
        Debug.Log("Thrust torque " + maxTorque);*/
    }


    private void SteerHelper()
    {
        for (int i = 0; i < number_of_wheels; i++)
        {
            WheelHit wheelhit;
            m_WheelColliders[i].GetGroundHit(out wheelhit);
            if (wheelhit.normal == Vector3.zero)
                return; // wheels arent on the ground so dont realign the rigidbody velocity
        }

        // this if is needed to avoid gimbal lock problems that will make the car suddenly shift direction
        if (Mathf.Abs(m_OldRotation - transform.eulerAngles.y) < 10f)
        {
            var turnadjust = (transform.eulerAngles.y - m_OldRotation) * m_SteerHelper;
            Quaternion velRotation = Quaternion.AngleAxis(turnadjust, Vector3.up);
            if (m_Rigidbody!=null)
            m_Rigidbody.velocity = velRotation * m_Rigidbody.velocity;
        }
        m_OldRotation = transform.eulerAngles.y;
    }


    // this is used to add more grip in relation to speed
    private void AddDownForce()
    {
        m_WheelColliders[0].attachedRigidbody.AddForce(-transform.up * m_Downforce *
                                                     m_WheelColliders[0].attachedRigidbody.velocity.magnitude);
    }


    // checks if the wheels are spinning and is so does three things
    // 1) emits particles
    // 2) plays tiure skidding sounds
    // 3) leaves skidmarks on the ground
    // these effects are controlled through the WheelEffects class
    private void CheckForWheelSpin()
    {
        // loop through all wheels
        for (int i = 0; i < number_of_wheels; i++)
        {
            WheelHit wheelHit;
            m_WheelColliders[i].GetGroundHit(out wheelHit);

            // is the tire slipping above the given threshhold
            /*if (Mathf.Abs(wheelHit.forwardSlip) >= m_SlipLimit || Mathf.Abs(wheelHit.sidewaysSlip) >= m_SlipLimit)
            {
                m_WheelEffects[i].EmitTyreSmoke();

                // avoiding all four tires screeching at the same time
                // if they do it can lead to some strange audio artefacts
                if (!AnySkidSoundPlaying())
                {
                    m_WheelEffects[i].PlayAudio();
                }
                continue;
            }

            // if it wasnt slipping stop all the audio
            if (m_WheelEffects[i].PlayingAudio)
            {
                m_WheelEffects[i].StopAudio();
            }
            // end the trail generation
            m_WheelEffects[i].EndSkidTrail();*/
        }
    }

    // crude traction control that reduces the power to wheel if the car is wheel spinning too much
    private void TractionControl()
    {
        WheelHit wheelHit;
        switch (m_CarDriveType)
        {
            case CarDriveType.FourWheelDrive:
                // loop through all wheels
                for (int i = 0; i < number_of_wheels; i++)
                {
                    m_WheelColliders[i].GetGroundHit(out wheelHit);

                    AdjustTorque(wheelHit.forwardSlip);
                }
                break;

            case CarDriveType.RearWheelDrive:
                m_WheelColliders[number_of_wheels - 1].GetGroundHit(out wheelHit);
                AdjustTorque(wheelHit.forwardSlip);

                m_WheelColliders[number_of_wheels - 2].GetGroundHit(out wheelHit);
                AdjustTorque(wheelHit.forwardSlip);
                break;

            case CarDriveType.FrontWheelDrive:
                m_WheelColliders[0].GetGroundHit(out wheelHit);
                AdjustTorque(wheelHit.forwardSlip);
                if (number_of_wheels > 3)
                {
                    m_WheelColliders[1].GetGroundHit(out wheelHit);
                    AdjustTorque(wheelHit.forwardSlip);
                }
                break;
        }
    }


    private void AdjustTorque(float forwardSlip)
    {
        if (forwardSlip >= m_SlipLimit && m_CurrentTorque >= 0)
        {
            m_CurrentTorque -= 10 * m_TractionControl;
        }
        else
        {
            m_CurrentTorque += 10 * m_TractionControl;
            if (m_CurrentTorque > m_FullTorqueOverAllWheels)
            {
                m_CurrentTorque = m_FullTorqueOverAllWheels;
            }
        }
    }


    /*private bool AnySkidSoundPlaying()
    {
        for (int i = 0; i < number_of_wheels; i++)
        {
            if (m_WheelEffects[i].PlayingAudio)
            {
                return true;
            }
        }
        return false;
    }*/
}
//}