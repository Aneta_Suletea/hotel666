﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntersectionSound : MonoBehaviour {

    // Use this for initialization
    private AudioSource sound;
	void Start () {
        sound = this.GetComponent<AudioSource>();
	}
    public void OnTriggerEnter(Collider other)
    {
        //Debug.Log("Object tag : " + other.gameObject.tag);
        if (other.gameObject.CompareTag("Player"))
        {
            sound.Play();
        }
    }
    // Update is called once per frame
    void Update () {
		
	}
}